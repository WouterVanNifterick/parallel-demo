unit ScapeyeParallel.Forms.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  System.Generics.Collections, System.Threading, FMX.Controls.Presentation,
  FMX.StdCtrls, FMX.ScrollBox, FMX.Memo, FMX.Layouts, FMX.ListBox,
  FMX.Objects;

type
  TImg = record
    Name: string;
    Col:TAlphaColor;
    Delay:integer;
    function ToString:string;
  end;

  TScapeyeThread = class(TThread)
  protected
    id:integer;
    Img:TImg;
    procedure Execute; override;
    constructor Create(q: TQueue<TImg>; d:TList<TImg>; id:integer);
  end;

  TfrmMain = class(TForm)
    tmr1: TTimer;
    lbl1: TLabel;
    memoProcessed: TMemo;
    tbProcessingTime: TTrackBar;
    lst1: TListBox;
    btn1: TButton;
    lbl2: TLabel;
    lbl3: TLabel;
    tbMaxQueueSize: TTrackBar;
    lbl4: TLabel;
    lbl5: TLabel;
    lblProcessed: TLabel;
    tbFrameRate: TTrackBar;
    lbl6: TLabel;
    lbl7: TLabel;
    memoIncoming: TMemo;
    memoProcessing: TMemo;
    lbl8: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure tmr1Timer(Sender: TObject);
    procedure tbProcessingTimeChange(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    procedure AddWorker;
    procedure CleanQueue;
    { Private declarations }
  public
    Queue    : TQueue<TImg>;
    DoneList : TList<TImg>;
    Workers  : TList<TScapeyeThread>;
    procedure FinishImg(Img:TImg);
    function Popimg(var Img:TImg):boolean;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

uses System.DateUtils;

procedure RemoveFromMemo(const memo:TMemo;const str:string);
begin
  if memo.lines.IndexOf(str)>=0 then
    memo.Lines.Delete(memo.lines.IndexOf(str));
end;

procedure TfrmMain.btn1Click(Sender: TObject);
begin
  AddWorker
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Queue    := TQueue<TImg>.Create;
  DoneList := TList<Timg>.Create;
  Workers  := TList<TScapeyeThread>.Create;
  tbProcessingTimeChange(self);

  AddWorker;
end;

function TfrmMain.Popimg(var Img: TImg): boolean;
begin
  Result := Queue.Count > 0;
  if Queue.Count=0 then
    Exit
  else
  begin
    Img := Queue.Extract;
    RemoveFromMemo(memoIncoming, Img.ToString);
    memoProcessing.Lines.Add(Img.ToString);
  end;
end;

// mimic an incoming image by creating it, and add it to the queue
procedure TfrmMain.tmr1Timer(Sender: TObject);
var
  Img: TImg;
begin
  Img.Name  := FormatDateTime('hh:nn:ss.zzz', now);
  Img.Col   := Random(MaxInt);
  Img.Delay := Round(tbProcessingTime.Value + ((-0.5 + Random)*tbProcessingTime.Value));

  Queue.Enqueue(Img);
  memoIncoming.Lines.Add(Img.ToString);
  lbl1.Text := 'Queue size: ' + Queue.Count.ToString;
end;

procedure TfrmMain.tbProcessingTimeChange(Sender: TObject);
begin
  lbl2.Text := Round(tbProcessingTime.Value).ToString + ' ms';
  lbl4.Text := Round(tbMaxQueueSize.Value).ToString + ' images';
  lbl6.Text := Round(tbFrameRate.Value).ToString + ' fps';
  tmr1.Interval := Round(1000/tbFrameRate.Value);
end;

procedure TfrmMain.AddWorker;
var
  w: TScapeyeThread;
begin
  w := TScapeyeThread.Create(Queue, DoneList, lst1.Items.Count);
  Workers.Add(w);
  lst1.Items.Add( 'Worker ' + w.id.ToString );
end;

procedure TfrmMain.CleanQueue;
var
  I: Integer;
  Img:TImg;
  Q:TQueue<TImg>;
begin
  // Clean Queue
  if Queue.Count < tbMaxQueueSize.Value then
    Exit;

  Q := TQueue<TImg>.Create;
  for I := 0 to Queue.Count do
    if Odd(I) then
    begin
      Img := Queue.Dequeue;
      Q.Enqueue(Img);
    end;
  FreeAndNil(Queue);
  Queue := Q;

  // Update GUI
  memoIncoming.Lines.Clear;
  for Img in Queue do
    memoIncoming.Lines.Add(Img.ToString)
end;

procedure TfrmMain.FinishImg(Img: TImg);
begin
  DoneList.Add(Img);
  RemoveFromMemo(memoProcessing, Img.ToString);
  memoProcessed.Lines.Add(Img.ToString);
  memoProcessed.GoToTextEnd;

  lblProcessed.Text := 'Processed: '+DoneList.Count.ToString;
  CleanQueue;
end;


{ TImg }

function TImg.ToString: string;
begin
  Result := Name + ' - ' + Delay.ToString
end;


{ TScapeyeThread }

constructor TScapeyeThread.Create(q: TQueue<TImg>; d:TList<TImg>; id:integer);
begin
  inherited Create(False);
  self.id := id;
end;


procedure TScapeyeThread.Execute;
var c:TDateTime; IsLoaded:boolean;

begin
  inherited;
  repeat
    // calling into main thread, and make sure that we're the
    // only one who popped this item, so synchronize this
    TThread.Synchronize( TThread.Current,
      procedure
      begin
        IsLoaded := frmMain.Popimg(Img);
      end);

    // apparently nothing was popped
    // so let's sleep so we don't flood the queue (main thread) with
    // requests, and quit this loop
    // maybe there will be something to do on the next iteration...
    if not IsLoaded then
    begin
      Sleep(1);
      Continue;
    end;

    // we've got our task now (the image to process)
    // mimic that we do some work by spinning around for a while
    c := now;
    repeat
      sleep(1);
    until MilliSecondSpan(now,c) >= Img.Delay;


    TThread.Synchronize( TThread.Current,
      procedure
      begin
        frmMain.FinishImg(Img);
      end );
  until Terminated;
end;


end.
