# Scapeye parallel demo #

This is a demonstration of the basic architecture that Scapeye needs to run multiple instances in parallel.

This now runs as a GUI program to give visual feedback about what's happening.
This should probably run as a Windows service or a daemon on Linux.


Instead of doing real work, it just loops for a given amount of time.
The Scapeye framework should do its recognition here. 

* Have a global queue for incoming requests
* Have worker threads look for work in that queue
* Make sure to synchronize access to that queue
* Build in a brief sleep when there's no work to do
* Have a cleanup function in case the queue gets full

![Screenshot](ScapeyeParallel.png "Screenshot")

